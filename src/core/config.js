export const APP_CONFIG = {
  appName: "Quick Payments",
  description: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",
};

export const API_CONFIG = {
  baseUrl: "https://dev-api.qs-pay.com/"
};
