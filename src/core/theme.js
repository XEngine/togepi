
const fontConfig = {
  default: {
    regular: {
      fontFamily: 'Inter-Regular',
      fontWeight: "400",
    },
    medium: {
      fontFamily: 'Inter-Black',
      fontWeight: "900",
    },
    light: {
      fontFamily: 'Inter-Light',
      fontWeight: "300",
    },
    thin: {
      fontFamily: 'Inter-Thin',
      fontWeight: "100",
    },
  },
};

export const theme = {
  //...DefaultTheme,
  //fonts: configureFonts(fontConfig),
  colors: {
    //...DefaultTheme.colors,
    primary: "#6960D8",
    white: "#FFFFFF",
    secondary: "#414757",
    error: "#f13a59",
    dark: "#414757",
    success: "#00B386",
  }
};
