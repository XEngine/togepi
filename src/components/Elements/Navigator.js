import React, { memo } from 'react';
import { TouchableHighlight, View, Text } from 'react-native';
import {Icon} from "@ui-kitten/components";

const Navigator = ({style, ...props}) => (
    <View style={{
        height: 70,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    }}>
        <TouchableHighlight style={{ marginLeft: 10, marginTop: 15 }}
                            onPress={() => { props.navigation.openDrawer() }}>
            <Icon name='people'/>
        </TouchableHighlight>
    </View>
);

export default memo(Navigator);
