import React, { memo } from 'react';
import { Image, StyleSheet } from 'react-native';

const Logo = ({style}) => (
  <Image
      source={require('../../assets/logo_v2.png')}
      style={[styles.image, style]}
      resizeMode="contain"/>
);

const styles = StyleSheet.create({
  image: {
    marginBottom: 12,
  },
});

export default memo(Logo);
