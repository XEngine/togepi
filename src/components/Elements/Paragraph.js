import React, { memo } from "react";
import {Text} from '@ui-kitten/components';
const Paragraph = ({ children, ...props }) =>
    <Text
        category='p1'
        {...props}
    >
      {children}
    </Text>;

export default memo(Paragraph);
