import React, { memo } from 'react';
import { Image, StyleSheet } from 'react-native';

const HeaderLogo = ({width}) => (
  <Image
      source={require('../../assets/headerLogo.png')}
      style={styles.image}
      resizeMode="contain"/>
);

const styles = StyleSheet.create({
  image: {
    width: 38,
    marginBottom: 12,
  },
});

export default memo(HeaderLogo);
