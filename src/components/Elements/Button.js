import React, {memo} from "react";
import {StyleSheet, View} from "react-native";
import {Button as ButtonComponent, Spinner} from '@ui-kitten/components';

const Button = ({mode, style, children, loading = false, ...props}) => (
    <ButtonComponent
        {...props}
        style={[styles.button, style]}
    >
        {children}
    </ButtonComponent>
);

const styles = StyleSheet.create({
    button: {
        width: "100%",
        margin: 5
    }
});

export default memo(Button);
