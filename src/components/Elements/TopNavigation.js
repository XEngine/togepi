import React, {memo} from "react";
import {Icon, Layout, TopNavigation, TopNavigationAction, Divider} from '@ui-kitten/components';
import {StyleSheet} from 'react-native';
import {getStatusBarHeight} from "react-native-status-bar-height";
import Logo from "../Elements/HeaderLogo";
import Navigator from "../Elements/Navigator";

const BackIcon = (props) => (
    <Icon {...props} name='arrow-ios-back'/>
);

const HeaderLogo = () => (
    <Logo width={100}/>
);

const UserIcon = (props) => (
    <Icon {...props} name='people'/>
);

const TopNavigations = ({goBack, disabledBack, disableRight, disableLogo, navigation}) => {
    const renderBackAction = () => {
        if (!disabledBack) {
            return (
                <>
                    <Navigator navigation={navigation}/>
                    <TopNavigationAction icon={BackIcon} onPress={goBack}/>
                </>
                )
        } else {
            return (
                <>
                    <Navigator navigation={navigation}/>
                    <TopNavigationAction />
                </>
            )
        }
    };


    const renderRightActions = () => (
        <>
        <TopNavigationAction icon={UserIcon}/>
        </>
    );

    return (
        <Layout style={styles.container} level='1'>
            <TopNavigation
                alignment='center'
                title={!disableLogo ? HeaderLogo: ""}
                accessoryLeft={renderBackAction}
                accessoryRight={!disableRight ? renderRightActions: ""}
            />
            <Divider />
        </Layout>
    );

};

const styles = StyleSheet.create({
    container: {
    },
});

export default memo(TopNavigations);
