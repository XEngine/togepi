import React, { memo } from "react";
import {Input, TopNavigationAction, Text} from '@ui-kitten/components';

const renderLabel = (text) => (
    <>
        <Text category='p2'>{text}</Text>
    </>
);

const TextInput = ({ errorText, ...props }) => (
    <Input
        {...props}
        label={renderLabel(props.label)}
        caption={errorText ? errorText : null}
        status={errorText ? "danger" : null}
        size="large"
    />
);

export default memo(TextInput);
