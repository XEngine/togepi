import React, {memo} from "react";
import {StyleSheet} from "react-native";
import {Text} from '@ui-kitten/components';

const Header = ({children, ...props}) =>
    <Text
        {...props}
        category='h4'
        style={styles.header}
    >
        {children}
    </Text>;

const styles = StyleSheet.create({
    header: {
        margin: 5,
        textAlign: "center"
    }
});


export default memo(Header);
