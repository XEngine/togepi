import React, {memo} from "react";
import {StyleSheet, View} from "react-native";
import {Text} from '@ui-kitten/components';
import { VictoryPie } from "victory-native";

const TopSellingProducts = ({mode, style, children, loading = false, ...props}) => (
    <>
        <Text category='h6'>Top Selling Products</Text>
        <VictoryPie
            renderInPortal={false}
            data={[
                { x: 100, y: 400 },
            ]}
            labels={({ datum }) => ``}
        />
    </>
);

const styles = StyleSheet.create({
    button: {
        width: "100%",
        margin: 5
    }
});

export default memo(TopSellingProducts);
