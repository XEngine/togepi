import React, {memo} from "react";
import {StyleSheet, View} from "react-native";
import {Text} from '@ui-kitten/components';
import {VictoryChart, VictoryTheme, VictoryBar, VictoryPie} from "victory-native";
const TopSellingCatalog = ({mode, style, children, loading = false, ...props}) => (
    <>
        <Text category='h6'>Top Selling Catalog</Text>
        <VictoryChart>
            <VictoryBar
                barRatio={0.8}
                style={{
                    data: { fill: "#c43a31" }
                }}
                data={[
                    { x: 1, y: 2 },
                    { x: 2, y: 3 },
                    { x: 3, y: 5 },
                    { x: 4, y: 4 },
                    { x: 5, y: 6 }
                ]}
            />
        </VictoryChart>
    </>
);

const styles = StyleSheet.create({
    button: {
        width: "100%",
        margin: 5
    }
});

export default memo(TopSellingCatalog);
