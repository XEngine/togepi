import React, {memo} from "react";
import {StyleSheet, KeyboardAvoidingView} from "react-native";
import {Dimensions, ScrollView, Platform, StatusBar} from "react-native";
const windowWidth = Dimensions.get('window').width;
import Constants from 'expo-constants';
StatusBar.setBarStyle("dark-content");

const FullScreenView = ({children}) => (
    <KeyboardAvoidingView behavior="height">
        {children}
    </KeyboardAvoidingView>
);

const styles = StyleSheet.create({
    container: {
        marginTop: -Constants.statusBarHeight
    }
});

export default memo(FullScreenView);
