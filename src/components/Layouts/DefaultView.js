import React, {memo, useEffect, useState} from "react";
import {Keyboard, KeyboardAvoidingView, Platform, StyleSheet, TouchableWithoutFeedback, View, Text} from "react-native";
import {get} from "../../api/endpoints/Merchant";
import storage from "../../api/storage";

const DefaultView = ({children, route}) => {
    const [isLoggedIn, setIsLoggedIn] = useState(null);
    const [merchant, setMerchant] = useState(null);
    const getMerchantProfile = async () => {
        return await get();
    };
    const isLogged = async () => {
       return await storage.get('authorization');
    };
    useEffect( () => {
         isLogged().then((response) => {
             if (response) {
                 setIsLoggedIn(response);
                 getMerchantProfile().then((res) => {
                     storage.set('merchant', JSON.stringify(res.result));
                     setMerchant(res.result);
                 })
             }
         });
    }, []);
    const renderChildren = (children) => {
       if (isLoggedIn) {
           return merchant ? children : null;
       } else {
           return children;
       }
    };
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : "height"}
            style={styles.container}
        >
            <View style={styles.inner}>
                {renderChildren(children)}
            </View>
        </KeyboardAvoidingView>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    inner: {
        flex: 1,
    },
    header: {
        fontSize: 36,
        marginBottom: 48
    }
});

export default memo(DefaultView);
