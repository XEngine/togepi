import { API_CONFIG } from '../core/config';
import storage from './storage';

class connector {
    /**
     *
     * @param url
     * @param params
     * @returns {Promise<Response>}
     */
    async get(url, params) {
        return await fetch(API_CONFIG.baseUrl + url, {
            method: 'GET',
            headers: await this._headers(),
            body: params
        }).then((response) => {
            return response.json();
        });
    }

    /**
     *
     * @param url
     * @param body
     * @returns {Promise<Response>}
     */
    async post(url, body) {
        return await fetch(API_CONFIG.baseUrl + url, {
            method: 'POST',
            headers: await this._headers(),
            body: JSON.stringify(body)
        }).then((response) => {
            return response.json();
        });
    }

    /**
     *
     * @returns {Promise<{Accept: string, "Content-Type": string}>}
     * @private
     */
    async _headers() {
        const requestHeader = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        };
        const authorization = await storage.get('authorization');
        if(authorization !== null) {
            requestHeader.authorization = `Bearer ${authorization}`;
        }
        return requestHeader;
    }
}

export default new connector();