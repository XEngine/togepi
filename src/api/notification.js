import Toast from "react-native-toast-message";

class notification {
    constructor() {
        this.config = {
            topOffset: 110,
            visibilityTime: 4000,
        };
    }

    /**
     *
     * @param message
     */
    error(message) {
        Toast.show({...this.config, type:"error", text1: "Error!", text2: message});
    }

    /**
     *
     * @param message
     */
    success(message) {
        Toast.show({...this.config, type:"success", text1: "Success!", text2: message});
    }

    /**
     *
     * @param message
     */
    info(message) {
        Toast.show({...this.config, type:"info", text1: "", text2: message});
    }
}

export default new notification();