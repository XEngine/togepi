import connector from "../connector";
const path = "Order";

export const getAll = async () => {
    return connector.post(`${path}/getall`, {orderType: 25});
};