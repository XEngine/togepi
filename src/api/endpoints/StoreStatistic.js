import connector from "../connector";
const path = "StoreStatistic";

/**
 *
 * @returns {Promise<Response>}
 */
export const getPreferredProduct = async () => {
    return connector.get(`${path}/PreferredProduct?BeginDate=2020-10-19T21:22&EndDate=2020-10-26T21:22&TopN=5`);
};

/**
 *
 * @returns {Promise<Response>}
 */
export const getPreferredCatalog = async () => {
    return connector.get(`${path}/PreferredCatalog?BeginDate=2020-10-19T21:22&EndDate=2020-10-26T21:22&TopN=5`);
};