import connector from "../connector";
import storage from "../storage";
const path = "Merchant";

/**
 *
 * @param UserName
 * @param Password
 * @returns {Promise<Response>}
 */
export const authenticate = async ({UserName, Password}) => {
    return connector.post(`${path}/Authenticate`, {
        UserName,
        Password,
    });
};

/**
 *
 * @param Email
 * @param Password
 * @param Name
 * @param PhoneNumber
 * @returns {Promise<Response>}
 */
export const register = async ({Email, Password, Name, PhoneNumber}) => {
    return connector.post(`${path}/Register`, {
        Email,
        Password,
        Name,
        PhoneNumber
    });
};

/**
 *
 * @param Email
 * @returns {Promise<Response>}
 */
export const forgotPassword = async ({Email}) => {
    return connector.post(`${path}/ForgotPassword`, {
        Email,
    });
};

/**
 *
 * @returns {Promise<void|SVGLength|string|SVGTransform|SVGPathSeg|SVGNumber|DOMPoint>}
 */
export const logout = async () => {
    await storage.remove('merchant');
    return await storage.remove('authorization');
};

/**
 *
 * @returns {Promise<*>}
 */
export const get = async () => {
    return connector.get(`${path}/get`);
};