import { Platform, NativeModules } from 'react-native'
import locales from "../locales/locales.json";

class localization {
    constructor() {
        this.lang = "en_US";
        this.defaultLanguage = "en_US";
        this.translations = {};
        this._detectLanguage();
        this._loadLanguageStrings();
    }

    /**
     *
     * @private
     */
    _detectLanguage() {
        this.lang = Platform.OS === 'ios'
            ? NativeModules.SettingsManager.settings.AppleLocale ||
            NativeModules.SettingsManager.settings.AppleLanguages[0]
            : NativeModules.I18nManager.localeIdentifier;
    }

    /**
     *
     * @private
     */
    _loadLanguageStrings() {
        if (!locales[this.lang]) {
            this.lang = this.defaultLanguage;
        }
        this.translations = locales[this.lang];
    }

    /**
     *
     * @param text
     * @returns {*|string}
     */
    get(text) {
        return this.translations[text] || "";
    }
}

export default new localization();