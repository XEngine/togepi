import AsyncStorage from '@react-native-community/async-storage';

class storage {
    /**
     *
     * @param key
     * @returns {Promise<*>}
     */
    async get(key) {
        return await AsyncStorage.getItem(key);
    }

    /**
     *
     * @param key
     * @param value
     * @returns {Promise<void>}
     */
    async set(key, value) {
        return AsyncStorage.setItem(key, value);
    }

    /**
     *
     * @param key
     * @returns {Promise<void|SVGLength|string|SVGTransform|SVGPathSeg|SVGNumber|DOMPoint>}
     */
    async remove(key) {
        return AsyncStorage.removeItem(key);
    }
}

export default new storage();