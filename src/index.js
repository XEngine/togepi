import React, {useEffect, useState, useRef} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Image, TouchableWithoutFeedback, View, StyleSheet, Text, Dimensions} from 'react-native';
import { Menu, MenuItem, Button } from '@ui-kitten/components';
import {logout} from "./api/endpoints/Merchant";

import Logo from './components/Elements/HeaderLogo'
const windowWidth = Dimensions.get('window').width;
const navigationRef = React.createRef();
import {
    HomeScreen,
    LoginScreen,
    RegisterScreen,
    ForgotPasswordScreen,
    AuthLoadingScreen,
    Dashboard,
    Orders
} from "./screens";

const Stack = createStackNavigator();


export default function App() {
    const [menuOpen, setMenuOpen] = useState(false);
    const handleMenu =() => {
        setMenuOpen(!menuOpen)
    }


    const navigateMenu = (name) => {
        navigationRef.current?.navigate(name);
        handleMenu();
    }

    const headerRight = () => (
        <TouchableWithoutFeedback onPress={() => alert('Under Construction')}>
            <Image source={require('./assets/user.png')} resizeMode="cover"
                   style={{resizeMode: "stretch", marginRight: 25}}/>
        </TouchableWithoutFeedback>
    );

    const headerLogo = () => (
        <Logo/>
    );

    const handleLogout = async () => {
        await logout();
        navigateMenu("Home");
    }

    const headerLeft = () => (
        <TouchableWithoutFeedback onPress={() => handleMenu()}>
            <Image source={require('./assets/menu.png')} resizeMode="cover"
                   style={{resizeMode: "stretch", marginLeft: 20}}/>
        </TouchableWithoutFeedback>
    );

    const RenderMenu = () => {
        if (menuOpen) {
            return (
                <View style={styles.menu} >
                    <Menu selectedIndex={1}>
                        <MenuItem onPress={() => navigateMenu("Dashboard")} title='Dashboard'/>
                        <MenuItem onPress={() => navigateMenu("Orders")} title='Orders'/>
                    </Menu>
                    <View>
                        <Button onPress={() => handleLogout()} status={"danger"}>Logout</Button>
                    </View>
                </View>
            );
        } else {
            return null;
        }
    }

    const RenderOverlay = () => {
        if (menuOpen) {
            return (
                <TouchableWithoutFeedback onPress={() => handleMenu()} >
                   <View style={[styles.overlay]}>
                   </View>
                </TouchableWithoutFeedback>
            );
        } else {
            return null;
        }
    }

    return (
        <>
            <NavigationContainer ref={navigationRef}>
                <Stack.Navigator initialRouteName="AuthLoading">
                    <Stack.Screen name="AuthLoading" component={AuthLoadingScreen}
                                  options={{headerShown: false, gestureEnabled: false}}/>
                    <Stack.Screen name="Home" component={HomeScreen} options={{headerShown: false, gestureEnabled: false}}/>
                    <Stack.Screen name="Login" component={LoginScreen}
                                  options={{headerShown: false, gestureEnabled: false}}/>
                    <Stack.Screen name="Register" component={RegisterScreen}
                                  options={{headerShown: false, gestureEnabled: false}}/>
                    <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen}
                                  options={{headerShown: false, gestureEnabled: false}}/>
                    <Stack.Screen name="Dashboard" component={Dashboard} options={{
                        gestureEnabled: false,
                        headerBackTitle: false,
                        headerLeft,
                        headerRight,
                        headerTitle: headerLogo
                    }}/>
                    <Stack.Screen name="Orders" component={Orders} options={{
                        gestureEnabled: false,
                        headerBackTitle: false,
                        headerLeft,
                        headerRight,
                        headerTitle: headerLogo
                    }}/>
                </Stack.Navigator>
            </NavigationContainer>
            <RenderOverlay/>
            <RenderMenu />
        </>
    );
}


const styles = StyleSheet.create({
    menu: {
        backgroundColor: "#FFFFFF",
        position: "absolute",
        left: 0,
        top: 0,
        height: "100%",
        width: windowWidth - 100,
        paddingTop: 100,
        paddingBottom: 30
    },
    overlay: {
        flex: 1,
        justifyContent: "space-evenly",
        opacity: 0.8,
        backgroundColor: "#000000",
        position: "absolute",
        left: 0,
        top: 0,
        height: "100%",
        width: "100%"
    }
});