import React, {memo, useState} from "react";
import {Text, StyleSheet, TouchableOpacity, View, ImageBackground} from "react-native";
import {emailValidator} from "../core/utils";
import DefaultView from "../components/Layouts/DefaultView";
import Header from "../components/Elements/Header";
import TextInput from "../components/Elements/TextInput";
import {theme} from "../core/theme";
import Button from "../components/Elements/Button";
import {forgotPassword} from "../api/endpoints/Merchant";
import Toast from "../components/Elements/Toast";
import {Layout} from "@ui-kitten/components";
import storage from "../api/storage";
import notification from "../api/notification";
import localization from "../api/localization";

const ForgotPasswordScreen = ({navigation}) => {
    const [email, setEmail] = useState({value: "", error: ""});
    const [loading, setLoading] = useState(false);
    const [toast, setToast] = useState({value: "", type: ""});

    const _onSendPressed = async () => {
        if (loading) return;

        const emailError = emailValidator(email.value);

        if (emailError) {
            setEmail({...email, error: emailError});
            return;
        }

        setLoading(true);

        const response = await forgotPassword({Email: email.value});

        if (response.succeeded === true) {
            notification.success(response.result);
            navigation.navigate("Login");
        } else {
            notification.error(response.message);
        }
        setLoading(false);
    };

    return (
        <DefaultView disableTop={true}>
            <ImageBackground  source={require('../assets/slide1.jpg')} style={styles.image}>
                <Layout style={styles.layout}>
                    <Header>Restore Password</Header>
                    <TextInput
                        style={styles.input}
                        label="E-mail address"
                        returnKeyType="done"
                        value={email.value}
                        onChangeText={text => setEmail({value: text, error: ""})}
                        error={!!email.error}
                        errorText={email.error}
                        autoCapitalize="none"
                        autoCompleteType="email"
                        textContentType="emailAddress"
                        keyboardType="email-address"
                        placeholder="Email Address"
                    />

                    <Button
                        loading={loading}
                        mode="contained"
                        onPress={_onSendPressed}
                        style={styles.button}
                    >
                        Send Reset Instructions
                    </Button>

                    <View style={styles.row}>
                        <TouchableOpacity onPress={() => navigation.navigate("Login")}>
                            <Text style={styles.link}>← Back to login</Text>
                        </TouchableOpacity>
                    </View>
                </Layout>
            </ImageBackground>
        </DefaultView>
    );
};

const styles = StyleSheet.create({
    back: {
        width: "100%",
        marginTop: 12
    },
    button: {
        marginTop: 15
    },
    label: {
        color: theme.colors.secondary,
        width: "100%"
    },
    layout: {
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "transparent",
        marginLeft: 30,
        marginRight: 30
    },
    input: {
        marginTop: 10
    },
    link: {
        marginTop: 10,
        fontWeight: "bold",
        color: theme.colors.primary
    },
    image: {
        flex: 1,
        justifyContent: "center",
    },
});

export default ForgotPasswordScreen;
