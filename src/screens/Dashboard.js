import React, {useEffect, useState} from "react";
import DefaultView from "../components/Layouts/DefaultView";
import {Button, List, ListItem, Text, Divider} from '@ui-kitten/components';
import {StyleSheet, SafeAreaView} from 'react-native';
import {getPreferredProduct, getPreferredCatalog} from "../api/endpoints/StoreStatistic";

const Dashboard = ({navigation}) => {
    const [data, setData] = useState([]);
    const [data2, setData2] = useState([]);
    const getPreferredProducts = async () => {
        return await getPreferredProduct();
    };
    const getPreferredCatalogs = async () => {
        return await getPreferredCatalog();
    };

    const renderItem = ({item}) => (
        <ListItem
            title={`${item.product}`}
            accessoryRight={evaProps => <Button disabled={true}>{item.numberOfProduct}</Button>}
        />
    );
    const renderItem2 = ({item}) => (
        <ListItem
            title={`${item.catalog}`}
            accessoryRight={evaProps => <Button disabled={true}>{item.numberOfProduct}</Button>}
        />
    );
    useEffect(() => {
        getPreferredProducts().then((response) => {
            setData(response.result[0].preferredProducts);
        });
        getPreferredCatalogs().then((response) => {
            setData2(response.result[0].preferredCatalogs);
        });
    }, []);
    return (
        <DefaultView>
            <SafeAreaView style={{marginTop: 20, marginBottom: 20}}>
                <Text category='h6' style={{margin: 20, marginTop: 0}}>Top Selling Products</Text>
                <List
                    style={styles.container}
                    data={data}
                    ItemSeparatorComponent={Divider}
                    renderItem={renderItem}
                />
                <Text category='h6' style={{margin: 20}}>Top Selling Catalog</Text>
                <List
                    style={styles.container}
                    data={data2}
                    ItemSeparatorComponent={Divider}
                    renderItem={renderItem2}
                />
            </SafeAreaView>

        </DefaultView>
    );
};

const styles = StyleSheet.create({
    container: {
        // height: "50%",
        backgroundColor: "#FFFFFF"
    },
    item: {
        marginBottom: 10
    }
});

export default Dashboard;
