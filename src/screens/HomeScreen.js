import React, { memo } from "react";
import { APP_CONFIG } from "../core/config";
import DefaultView from "../components/Layouts/DefaultView";
import Header from "../components/Elements/Header";
import Button from "../components/Elements/Button";
import Paragraph from "../components/Elements/Paragraph";
import {StyleSheet, View, ImageBackground} from "react-native";
import { Layout } from '@ui-kitten/components';
import Logo from "../components/Elements/Logo";

const HomeScreen = ({ navigation }) => (
  <DefaultView disableTop={true}>
      <ImageBackground  source={require('../assets/slide1.jpg')} style={styles.image}>

          <Layout style={styles.layout}>
              <Logo style={{width: 250 }}/>
              <Header>Welcome to {APP_CONFIG.appName}</Header>
              <Paragraph style={styles.paragraph}>
                  {APP_CONFIG.description}
              </Paragraph>
              <Button onPress={() => navigation.navigate("Login")}>
                  Login
              </Button>
              <Button
                  appearance='outline'
                  onPress={() => navigation.navigate("Register")}
              >
                  Sign Up
              </Button>
          </Layout>
      </ImageBackground>
  </DefaultView>
);

const styles = StyleSheet.create({
    paragraph: {
        textAlign: "center",
        margin: 10
    },
    image: {
        flex: 1,
        justifyContent: "center",
        paddingBottom: "40%"
    },
    layout: {
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "transparent",
        marginLeft: 30,
        marginRight: 30
    },
    view: {
        alignItems: "center"
    }
});

export default HomeScreen;
