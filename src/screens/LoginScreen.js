import React, {memo, useState} from "react";
import {TouchableOpacity, StyleSheet, View, ImageBackground} from "react-native";
import DefaultView from "../components/Layouts/DefaultView";
import Header from "../components/Elements/Header";
import Button from "../components/Elements/Button";
import TextInput from "../components/Elements/TextInput";
import {theme} from "../core/theme";
import {emailValidator, passwordValidator} from "../core/utils";
import {authenticate} from "../api/endpoints/Merchant";
import notification from '../api/notification';
import localization from '../api/localization';
import storage from '../api/storage';
import {Layout, Text} from "@ui-kitten/components";

const LoginScreen = ({navigation, route}) => {

    const [email, setEmail] = useState({value: "", error: ""});
    const [password, setPassword] = useState({value: "", error: ""});
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");

    const _onLoginPressed = async () => {
        if (loading) return;

        const emailError = emailValidator(email.value);
        const passwordError = passwordValidator(password.value);

        if (emailError || passwordError) {
            setEmail({...email, error: emailError});
            setPassword({...password, error: passwordError});
            return;
        }

        setLoading(true);

        const response = await authenticate({
            UserName: email.value,
            Password: password.value
        });
        if (response.succeeded === true) {
            const storeData = async (key, value) => {
                try {
                    await storage.set(key, value);
                    navigation.reset({index: 0, routes: [{ name: 'Dashboard' }]});
                    navigation.navigate("Dashboard")
                } catch (e) {
                    setError(response.message)
                    notification.error(localization.get(response.message));
                }
            }
            await storeData('authorization', response.result.accessToken);
        } else {
            setError(response.message)
            notification.error(localization.get(response.message));
        }
        setLoading(false);
    };
    return (
        <DefaultView route={route}>
            <ImageBackground  source={require('../assets/slide1.jpg')} style={styles.image}>
                <Layout style={styles.layout}>
                    <Header>Login</Header>
                    <TextInput
                        style={styles.input}
                        label='Email Address'
                        returnKeyType="next"
                        value={email.value}
                        onChangeText={text => setEmail({value: text, error: ""})}
                        error={!!email.error}
                        errorText={email.error}
                        autoCapitalize="none"
                        autoCompleteType="email"
                        textContentType="emailAddress"
                        placeholder="Email Address"
                        keyboardType="email-address"
                    />
                    <TextInput
                        style={styles.input}
                        label="Password"
                        returnKeyType="done"
                        value={password.value}
                        onChangeText={text => setPassword({value: text, error: ""})}
                        error={!!password.error}
                        errorText={password.error}
                        secureTextEntry
                        placeholder="Password"
                        autoCapitalize="none"
                    />
                    <View style={styles.forgotPassword}>
                        <TouchableOpacity
                            onPress={() => navigation.navigate("ForgotPassword")}
                        >
                            <Text>Forgot your password?</Text>
                        </TouchableOpacity>
                    </View>
                    <Button loading={loading} mode="contained" onPress={_onLoginPressed}>
                        Login
                    </Button>

                    <View style={styles.row}>
                        <Text style={styles.label}>Don’t have an account? </Text>
                        <TouchableOpacity onPress={() => navigation.navigate("Register")}>
                            <Text style={styles.link}>Sign Up</Text>
                        </TouchableOpacity>
                    </View>
                </Layout>
            </ImageBackground>

        </DefaultView>
    );
};

const styles = StyleSheet.create({
    input: {
        marginBottom: 10
    },
    imageLayout: {
        height: 200
    },
    image: {
        flex: 1,
        justifyContent: "center",
    },
    layout: {
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "transparent",
        marginLeft: 30,
        marginRight: 30
    },
    forgotPassword: {
        width: "100%",
        alignItems: "flex-end",
        marginBottom: 10
    },
    signup: {
        width: "100%",
        alignItems: "center",
        marginTop: 15
    },
    row: {
        flexDirection: "row",
        marginTop: 4
    },
    label: {
        color: theme.colors.secondary
    },
    link: {
        fontWeight: "bold",
        color: theme.colors.primary
    }
});

export default LoginScreen;
