import React, {memo} from "react";
import {Layout, Spinner} from '@ui-kitten/components';
import DefaultView from "../components/Layouts/DefaultView";
import storage from '../api/storage';
import {StyleSheet} from "react-native";

const AuthLoadingScreen = ({navigation, route}) => {
    const authToken = async () => {
        try {
            const value = await storage.get('authorization');
            if (value !== null) {
                navigation.navigate("Dashboard");
            } else {
                navigation.navigate("Home");
            }
        } catch (e) {
            navigation.navigate("Home");
        }
    }
    authToken();
    return (
        <DefaultView route={route}>
            <Layout style={styles.layout}>
                <Spinner/>
            </Layout>
        </DefaultView>
    );
};

const styles = StyleSheet.create({
    layout: {
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        marginLeft: 30,
        marginRight: 30,
        height:"100%"
    }
});

export default AuthLoadingScreen;
