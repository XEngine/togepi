import React, {useEffect, useState} from "react";
import DefaultView from "../components/Layouts/DefaultView";
import {Button, List, ListItem, Text, Divider, Card} from '@ui-kitten/components';
import {StyleSheet, SafeAreaView} from 'react-native';
import {getAll} from "../api/endpoints/Order";

const Orders = ({navigation}) => {
    const [data, setData] = useState([]);
    const getOrders = async () => {
        return await getAll();
    };

    const statusMap = {
        Refund: "danger",
        Paid: "success",
        Pending: "warning"
    };

    const renderItem = ({item}) => {
        return (
            <Card style={styles.item} status={statusMap[item.stateName] || "basic"}>
                <Text><Text style={{fontWeight: "bold"}}>Reference Number :</Text> {item.orderCode}</Text>
                <Text><Text style={{fontWeight: "bold"}}>Customer Email:</Text> {item.customerEmail}</Text>
                <Text><Text style={{fontWeight: "bold"}}>Authorization Date :</Text> {item.createdAt}</Text>
                <Text><Text style={{fontWeight: "bold"}}>State :</Text> {item.stateName}</Text>
                <Text><Text style={{fontWeight: "bold"}}>Amount :</Text> {item.amount} €</Text>
            </Card>
        );
    };

    useEffect(() => {
        getOrders().then((response) => {
            console.log("response", response);
            setData(response.result);
        });
    }, []);
    return (
        <DefaultView>
            <SafeAreaView style={{marginTop: 20, marginBottom: 20}}>
                <Text category='h6' style={{margin: 20, marginTop: 0}}>Orders</Text>
                <List
                    style={styles.container}
                    data={data}
                    ItemSeparatorComponent={Divider}
                    renderItem={renderItem}
                />
            </SafeAreaView>

        </DefaultView>
    );
};

const styles = StyleSheet.create({
    container: {
        height: "100%",
        backgroundColor: "#FFFFFF"
    },
    item: {
        marginBottom: 10
    }
});

export default Orders;
