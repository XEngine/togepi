import React, {memo, useState} from "react";
import {View, StyleSheet, TouchableOpacity, ImageBackground} from "react-native";
import DefaultView from "../components/Layouts/DefaultView";
import Header from "../components/Elements/Header";
import Button from "../components/Elements/Button";
import TextInput from "../components/Elements/TextInput";
import {theme} from "../core/theme";
import {emailValidator, passwordValidator} from "../core/utils";
import {register} from "../api/endpoints/Merchant";
import {Layout, Text} from "@ui-kitten/components";
import notification from "../api/notification";

const RegisterScreen = ({navigation}) => {
    const [name, setName] = useState({value: "", error: ""});
    const [email, setEmail] = useState({value: "", error: ""});
    const [password, setPassword] = useState({value: "", error: ""});
    const [password2, setPassword2] = useState({value: "", error: ""});
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");

    const _onSignUpPressed = async () => {
        if (loading) return;

        const emailError = emailValidator(email.value);
        const passwordError = passwordValidator(password.value);
        const password2Error = passwordValidator(password2.value);

        if (emailError || passwordError) {
            setEmail({...email, error: emailError});
            setPassword({...password, error: passwordError});
            setPassword2({...password2, error: password2Error});
            return;
        }

        setLoading(true);

        const response = await register({
            Name: name.value,
            Email: email.value,
            Password: password.value,
            ConfirmPassword: password2.value
        });
        if (response.succeeded === true) {
            notification.success(response.result);
            navigation.navigate("Login");
        } else {
            setError(response.message)
            notification.error(response.message);
        }
        setLoading(false);
    };

    return (
        <DefaultView disableTop={true}>
            <ImageBackground  source={require('../assets/slide1.jpg')} style={styles.image}>
                <Layout style={styles.layout}>
                    <Header>Create Account</Header>

                    <TextInput
                        style={styles.input}
                        label="Name"
                        returnKeyType="next"
                        placeholder="Name"
                        value={name.value}
                        onChangeText={text => setName({value: text, error: ""})}
                        error={!!name.error}
                        errorText={name.error}
                    />

                    <TextInput
                        style={styles.input}
                        label="Email"
                        returnKeyType="next"
                        value={email.value}
                        onChangeText={text => setEmail({value: text, error: ""})}
                        error={!!email.error}
                        errorText={email.error}
                        autoCapitalize="none"
                        autoCompleteType="email"
                        placeholder="Email Address"
                        textContentType="emailAddress"
                        keyboardType="email-address"
                    />

                    <TextInput
                        style={styles.input}
                        label="Password"
                        returnKeyType="done"
                        value={password.value}
                        onChangeText={text => setPassword({value: text, error: ""})}
                        error={!!password.error}
                        errorText={password.error}
                        placeholder="Password"
                        secureTextEntry
                        autoCapitalize="none"
                    />

                    <TextInput
                        style={styles.input}
                        label="Confirm Password"
                        returnKeyType="done"
                        value={password2.value}
                        onChangeText={text => setPassword2({value: text, error: ""})}
                        error={!!password2.error}
                        errorText={password2.error}
                        placeholder="Confirm Password"
                        secureTextEntry
                        autoCapitalize="none"
                    />

                    <Button
                        loading={loading}
                        mode="contained"
                        onPress={_onSignUpPressed}
                    >
                        Sign Up
                    </Button>

                    <View style={styles.row}>
                        <Text style={styles.label}>Already have an account? </Text>
                        <TouchableOpacity onPress={() => navigation.navigate("Login")}>
                            <Text style={styles.link}>Login</Text>
                        </TouchableOpacity>
                    </View>
                </Layout>
            </ImageBackground>
        </DefaultView>
    );
};

const styles = StyleSheet.create({
    input: {
        marginBottom: 10
    },
    layout: {
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "transparent",
        marginLeft: 30,
        marginRight: 30
    },
    label: {
        color: theme.colors.secondary
    },
    row: {
        flexDirection: "row",
        marginTop: 4
    },
    link: {
        fontWeight: "bold",
        color: theme.colors.primary
    },
    image: {
        flex: 1,
        justifyContent: "center",
    },
});

export default RegisterScreen;
