import React from "react";
import { StatusBar } from "react-native";
import App from "./src";
import * as eva from '@eva-design/eva';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import Toast from 'react-native-toast-message';

StatusBar.setBarStyle("dark-content");

const Main = () => (
    <>
        <IconRegistry name="eva" icons={EvaIconsPack} />
        <ApplicationProvider {...eva} theme={eva.light}>
            <App />
            <Toast ref={(ref) => Toast.setRef(ref)} />
        </ApplicationProvider>
    </>
);

export default Main;
